<html lang="es">

<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="style.css">
    <title>La dulce Vida</title>
</head>

<body>
    <header class="header">
        <figure class="container">
            <img src="Logo.jpg" class="responsive">
        </figure>
    </header>

    <nav class="nav">
        <div class="container">
            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="quienes.html">¿Quienes Somos?</a></li>
                <li><a href="contacto.html">Contacto</a></li>
            </ul>
        </div>
    </nav>

    <main class="main">

<section class="qt">
    <h2>¿Quieres?</h2>
</section>

<section class="qq">

    <article class="ql">
        <p>
            El pastel está elaborado de crepas de colores y crema batida, con la finalidad de que nuestros
            consumidores decoren su rebanada o pastel como ellos los prefieran.
            La forma del pastel será redonda aproximadamente de 18 cm de diámetro x 8 de alto sin decorado.
        </p>
    </article>


    <div class="Video" style="text-align:center; padding-top:150px; ">
        <video controls>
            <source src="Crepas.mp4" type="video/mp4">
            Tu navegador no implementa el elemento <code>video</code>.
        </video>
    </div>
</section>

<section class="dt">
    <h2>Descripcion del Producto</h2>
</section>

<section class="q">
    <article class="ql11">
        <strong>Porciones: </strong>12 rebanadas aproximadamente
        <strong>Relleno: </strong> crepas y crema batida
        <br> 
        <strong>Precio por rebanada de:</strong>
       $ 25
    </article>


    <figure class="qr">
        <img src="c1.jpeg" alt="Figura1" width="600px" height="400px">
    </figure>
</section>

<section class="dt">
    <h2>Colores</h2>
</section>

<section class="cls">
    <article class="cl">
        <strong>Crema Blanca contiene: </strong>
        <ul>
            <li>
                Azul
            </li>
            <li>
                Morado
            </li>
            <li>
                Verde
            </li>
            <li>
                Amarillo
            </li>
            <li>
                Naranja
            </li>
            <li>
                Rojo
            </li>
        </ul>
    </article>

    <article class="cla">
        <strong>Crema Negra contiene: </strong>
        <p>Tonalidades de azul fuerte hasta blanco</p>
    </article>
</section>
</main>

    <footer class="container">
        <div class="row">
            <p class="col-sm-4">© La dulce Vida, 2019</p>
        </div>
    </footer>

</body>

</html>